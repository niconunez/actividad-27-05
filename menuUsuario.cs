﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using MySql.Data.MySqlClient;

namespace Ejercicio1
{
    public class menuUsuario
    {
        public List<Publicacion> listaPublis;


        public void ListarMenu()
        {
            string opt = "0";

            listaPublis = new List<Publicacion>();
            do
            {
                Console.Clear();
            Console.WriteLine("¡Bienvenido usuario!");
            Console.WriteLine("---------------------------------");
            Console.WriteLine("Digite 1 para crear publicacion, 2 para enviar ticket al soporte y 3 para comprar telefono");
            opt = Console.ReadLine();

            switch (opt)
            {
                case "1":
                    Console.WriteLine("Bienvenido a Crear Publicación");
                    Publicacion newPubli = new Publicacion();

                    Console.WriteLine("Id: ");
                    newPubli.Id = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Id_user: ");
                    newPubli.Id_user = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Marca: ");
                    newPubli.Marca = Console.ReadLine();
                    Console.WriteLine("Modelo: ");
                    newPubli.Modelo = Console.ReadLine();
                    Console.WriteLine("Estado: ");
                    newPubli.Estado = Console.ReadLine();
                    Console.WriteLine("Precio: ");
                    newPubli.Precio = Convert.ToInt32(Console.ReadLine());
                    newPubli.Aprobado = 0;
                    listaPublis.Add(newPubli);
                    Console.Clear();
                    Console.WriteLine("Publicacion enviada con exito! En espera de aprobación.");

                    ConexionBD.obtenerConexion();
                    MySqlCommand comandopubli = new MySqlCommand(String.Format("insert into publicacion(id,id_user,marca,modelo,estado,precio,aprobado)values('{0}','{1}','{2}','{3}','{4}','{5}','{6}')", newPubli.Id, newPubli.Id_user, newPubli.Marca, newPubli.Modelo, newPubli.Estado, newPubli.Precio, newPubli.Aprobado), ConexionBD.obtenerConexion());
                    comandopubli.ExecuteNonQuery();
                    Console.WriteLine("Presione ENTER para volver al menu principal");
                    Console.ReadLine();
                    break;
                case "2":

                    Console.WriteLine("Bienvenido a Soporte al Cliente");
                    Ticket newTicket = new Ticket();

                    Console.WriteLine("Ingrese su ID de usuario: ");
                    newTicket.Id_user = Convert.ToInt32(Console.ReadLine());
                    Console.WriteLine("Ingrese su problema. Sea lo más especifico posible.");
                    newTicket.Descripcion = Console.ReadLine();
                    newTicket.Solucion = "";
                    Console.WriteLine("¡Ticket enviado con éxito! Pronto un administrador le responderá.");

                    ConexionBD.obtenerConexion();
                    MySqlCommand comandoticket = new MySqlCommand(String.Format("insert into ticket(id_user,descripcion,solucion)values('{0}','{1}','{2}')", newTicket.Id_user, newTicket.Descripcion, newTicket.Solucion), ConexionBD.obtenerConexion());
                    comandoticket.ExecuteNonQuery();
                    Console.WriteLine("Presione ENTER para volver al menu principal");
                    Console.ReadLine();

                    break;


                case "3":
                    Console.WriteLine("Bienvenido a comprar telefono");
                    List<Publicacion> listarPublis = new List<Publicacion>();
                    MySqlCommand selectpublisComp = new MySqlCommand(String.Format("select * from publicacion"), ConexionBD.obtenerConexion());
                    MySqlDataReader readerps = selectpublisComp.ExecuteReader();
                    Publicacion p = new Publicacion();
                    

                    
                        while (readerps.Read())
                        {
                            p.Id = readerps.GetInt32(0);
                            p.Id_user = readerps.GetInt32(1);
                            p.Marca = readerps.GetString(2);
                            p.Modelo = readerps.GetString(3);
                            p.Estado = readerps.GetString(4);
                            p.Precio = readerps.GetInt32(5);
                            p.Aprobado = readerps.GetInt32(6);

                            listarPublis.Add(p);
                            if (p.Aprobado == 1)
                            {
                                Console.WriteLine("Id: " + p.Id);
                                Console.WriteLine("Marca: " + p.Marca);
                                Console.WriteLine("Modelo: " + p.Modelo);
                                Console.WriteLine("Estado: " + p.Estado);
                                Console.WriteLine("Precio: " + p.Precio);
                                Console.WriteLine("                   ");
                            }
                        }

                        Boleta newBoleta = new Boleta();
                        Console.WriteLine("Seleccione el id del telefono que desea comprar: ");
                        newBoleta.Id_publi = Convert.ToInt32(Console.ReadLine());

                        Console.WriteLine("Escriba su nombre de usuario: ");
                        newBoleta.User = Console.ReadLine();


                        ConexionBD.obtenerConexion();
                        MySqlCommand comandoboleta = new MySqlCommand(String.Format("insert into boleta(user,id_publi)values('{0}','{1}')", newBoleta.User, newBoleta.Id_publi), ConexionBD.obtenerConexion());
                        comandoboleta.ExecuteNonQuery();

                        MySqlCommand borraPubliComprada = new MySqlCommand(String.Format("delete from publicacion where id='{0}'", newBoleta.Id_publi), ConexionBD.obtenerConexion());
                        borraPubliComprada.ExecuteNonQuery();
                        Console.Clear();
                        Console.WriteLine("¡Telefono comprado y boleta generada! Muchas gracias por comprar en Sell-Phone.");
                        Console.WriteLine("-----------------------------------------------");
                        Console.WriteLine("Presione ENTER para volver al menu principal");
                        Console.ReadLine();

                        break;

            }

            } while (opt != "0");



        }
    }
}