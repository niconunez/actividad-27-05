﻿using System;

namespace Ejercicio1
{
    class Program
    {
        public static void Main(string[] args)
        {
            string opcion = "0";
            
            
            do
            {
                Console.WriteLine("¿Desea ingresar como 1: Usuario o 2: Administrador?");
                opcion = Console.ReadLine();

                switch (opcion)
                {
                    case "1":
                        Ejercicio1.menuLogin menul = new Ejercicio1.menuLogin();
                        menul.MostrarLogin();
                        Console.Clear();
                        break;

                    case "2":
                        Ejercicio1.menuAdmin menua = new Ejercicio1.menuAdmin();
                        menua.ListarMenu();
                        break;
                    case "3":
                        Console.Clear();
                        break;
                }

            } while (opcion != "0");
        }

        class Administrador
        {

        }

        class Usuario
        {
            private string nombre;
            private string apellido;
        }

        class Reporte
        {

        }

        class Publicacion
        {
            private string marca;
            private string modelo;
            private int precio;

            public Publicacion()
            {

            }

        }
    }
}
