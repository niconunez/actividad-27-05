﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio1
{
    class Ticket
    {
        int id;
        int id_user;
        string descripcion;
        string solucion;

        public int Id { get => id; set => id = value; }
        public int Id_user { get => id_user; set => id_user = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public string Solucion { get => solucion; set => solucion = value; }
    }
}
