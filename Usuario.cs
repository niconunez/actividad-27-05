﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio1
{
    class Usuario
    {
        string user;
        string pass;
        string nombre;
        string apellido;
        string direccion;
        string ciudad;

        
        public string Nombre { get => nombre; set => nombre = value; }
        public string Apellido { get => apellido; set => apellido = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Ciudad { get => ciudad; set => ciudad = value; }
        public string User { get => user; set => user = value; }
        public string Pass { get => pass; set => pass = value; }
    }
}
