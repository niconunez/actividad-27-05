﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio1
{
    public class Publicacion
    {
        private int id;
        private int id_user;
        private string marca;
        private string modelo;
        private string estado;
        private int precio;
        private int aprobado;

        public Publicacion()
        {
        }

        public int Id { get => id; set => id = value; }
        public int Id_user { get => id_user; set => id_user = value; }
        public string Marca { get => marca; set => marca = value; }
        public string Modelo { get => modelo; set => modelo = value; }
        public string Estado { get => estado; set => estado = value; }
        public int Precio { get => precio; set => precio = value; }
        public int Aprobado { get => aprobado; set => aprobado = value; }

        
        


    }
}
