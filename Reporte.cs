﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio1
{
    class Reporte
    {
        private int id;

        private int id_user;
        private string descripcion;

        public Reporte()
        {

        }

        public Reporte(int id, int id_user, string descripcion)
        {
            this.id = id;
            this.id_user = id_user;
            this.descripcion = descripcion;
        }

        public int Id { get => id; set => id = value; }
        public int Id_user { get => id_user; set => id_user = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
    }
    
}
