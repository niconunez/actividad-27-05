﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Ejercicio1
{
    public class menuAdmin
    {
        private List<Reporte> listaReportes;
        List<Publicacion> listaPublis = new List<Publicacion>();



        

        public void ListarMenu()
        {   

            
            string opt = "0";
            
            listaReportes = new List<Reporte>();
            
            do
            {
                Console.WriteLine("¡Bienvenido administrador!");
                Console.WriteLine("---------------------------------");
                Console.WriteLine("Digite 1 para Generar un Reporte, 2 para Aprobar publicaciones, 3 para Atencion al Cliente y 4 para Listar Reportes");
                opt = Console.ReadLine();
                
                switch (opt)
                {
                    case "1":
                        Console.Clear();
                        Console.WriteLine("Bienvenido a Generar Reportes");
                        Reporte newReporte = new Reporte();
                        
                        Console.WriteLine("Id: ");
                        newReporte.Id = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Id_user: ");
                        newReporte.Id_user = Convert.ToInt32(Console.ReadLine());
                        Console.WriteLine("Descripcion: ");
                        newReporte.Descripcion = Console.ReadLine();
                        Console.WriteLine("¡Reporte creado exitosamente!");
                        Console.WriteLine("                   ");
                        Console.WriteLine("---------------------------------");
                        listaReportes.Add(newReporte);
                        ConexionBD.obtenerConexion();
                        MySqlCommand comando = new MySqlCommand(String.Format("insert into reporte(id,id_user,descripcion)values('{0}','{1}','{2}')",newReporte.Id,newReporte.Id_user,newReporte.Descripcion),ConexionBD.obtenerConexion());

                        comando.ExecuteNonQuery();

                        Console.WriteLine("Presione ENTER para volver al menu principal");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                        
                    case "2":
                        Console.Clear();
                        Console.WriteLine("Bienvenido a Aprobar publicaciones");
                        List<Publicacion> aprobarPublis = new List<Publicacion>();
                        
                        MySqlCommand selectpublis = new MySqlCommand(String.Format("select * from publicacion"), ConexionBD.obtenerConexion());
                        MySqlDataReader readerp = selectpublis.ExecuteReader();
                        Publicacion p = new Publicacion();


                        while (readerp.Read())
                        {
                            p.Id = readerp.GetInt32(0);
                            p.Id_user = readerp.GetInt32(1);
                            p.Marca = readerp.GetString(2);
                            p.Modelo = readerp.GetString(3);
                            p.Estado = readerp.GetString(4);
                            p.Precio = readerp.GetInt32(5);
                            p.Aprobado = readerp.GetInt32(6);

                            aprobarPublis.Add(p);

                            Console.WriteLine("Id: " + p.Id);
                            Console.WriteLine("Id_user: " + p.Id_user);
                            Console.WriteLine("Marca: " + p.Marca);
                            Console.WriteLine("Modelo: " + p.Modelo);
                            Console.WriteLine("Estado: " + p.Estado);
                            Console.WriteLine("Precio: " + p.Precio);
                            Console.WriteLine("Aprobado: " + p.Aprobado);
                            Console.WriteLine("                   ");


                        }
                        Console.WriteLine("Seleccione el id de la publicación a aprobar: ");
                        int idaprob = Convert.ToInt32(Console.ReadLine());
                        ConexionBD.obtenerConexion();
                        MySqlCommand updatepubli = new MySqlCommand(String.Format("UPDATE publicacion SET aprobado=1 WHERE id='{0}'",idaprob),ConexionBD.obtenerConexion());

                        updatepubli.ExecuteNonQuery();

                        Console.WriteLine("¡Publicacion aprobada con exito!");
                        Console.ReadLine();

                        /*foreach (Publicacion p in listaPublis)
                        {
                            Console.WriteLine("Id: " + p.Id);
                            Console.WriteLine("Id_user: " + p.Id_user);
                            Console.WriteLine("Marca: " + p.Marca);
                            Console.WriteLine("Modelo: " + p.Modelo);
                            Console.WriteLine("Estado: " + p.Estado);
                            Console.WriteLine("Precio: " + p.Precio);
                            Console.WriteLine("                   ");


                            newPubli.Id = p.Id;
                            newPubli.Id_user = p.Id_user;
                            newPubli.Marca = p.Marca;
                            newPubli.Modelo = p.Modelo;
                            newPubli.Estado = p.Estado;
                            newPubli.Precio = p.Precio;
                            Console.WriteLine("Digite 1 si desea aprobar esta publicacion");                   
                            newPubli.Aprobado = Convert.ToInt32(Console.ReadLine());
                            listaPublis.Add(newPubli);
                        }
                        */


                        break;
                    case "3":
                        Console.Clear();
                        Console.WriteLine("Bienvenido a Atencion al cliente");
                        List<Ticket> listaTicket = new List<Ticket>();
                        MySqlCommand selecttickets = new MySqlCommand(String.Format("select * from ticket"), ConexionBD.obtenerConexion());
                        MySqlDataReader readert = selecttickets.ExecuteReader();
                        Ticket t = new Ticket();
                        while (readert.Read())
                        {
                            t.Id = readert.GetInt32(0);
                            t.Id_user = readert.GetInt32(1);
                            t.Descripcion = readert.GetString(2);
                            t.Solucion = readert.GetString(3);


                            listaTicket.Add(t);

                            Console.WriteLine("Id: " + t.Id);
                            Console.WriteLine("Id_user: " + t.Id_user);
                            Console.WriteLine("Descripcion: " + t.Descripcion);
                            Console.WriteLine("Solución: " + t.Solucion);

                            Console.WriteLine("                   ");
                        }
                        Console.WriteLine("Seleccione el id del ticket a responder: ");
                        Console.WriteLine("Digite 0 para volver al menu principal");
                        int idticket = Convert.ToInt32(Console.ReadLine());
                        if(idticket == 0)
                        {
                            Console.Clear();
                            break;
                        }
                            
                        Console.Clear();
                        Console.WriteLine("Respondiendo al ticket numero " + idticket);
                        MySqlCommand descticket = new MySqlCommand(String.Format("select descripcion from ticket WHERE id='{0}'", idticket), ConexionBD.obtenerConexion());
                                                
                        MySqlDataReader readerdesct = descticket.ExecuteReader();
                        
                        Ticket d = new Ticket();
                        while (readerdesct.Read())
                        {                                       
                            d.Descripcion = readerdesct.GetString(0);
                            listaTicket.Add(d);
                            Console.WriteLine("Problema del usuario: " + d.Descripcion);  
                        }

                        Console.WriteLine("Escriba la solución al problema del usuario: ");
                        string soluticket = Console.ReadLine();

                        ConexionBD.obtenerConexion();
                        MySqlCommand updateticket = new MySqlCommand(String.Format("UPDATE ticket SET solucion='{0}' WHERE id='{1}'", soluticket,idticket), ConexionBD.obtenerConexion());

                        updateticket.ExecuteNonQuery();
                        Console.WriteLine("");
                        Console.WriteLine("¡Ticket respondido con exito!");
                        Console.WriteLine("Presione enter para volver al menu principal");
                        Console.ReadLine();
                        Console.Clear();

                        break;
                    case "4":
                        Console.Clear();

                        Console.WriteLine("Bienvenido a Listar Reportes");
                        Console.WriteLine("Estos son los reportes ingresados: ");
                        Console.WriteLine("  ");
                        List<Reporte> lista = new List<Reporte>();
                        MySqlCommand comandoselect = new MySqlCommand(String.Format("select * from reporte"), ConexionBD.obtenerConexion());
                        MySqlDataReader reader = comandoselect.ExecuteReader();
                        Reporte r = new Reporte();
                        while (reader.Read())
                        {
                            r.Id = reader.GetInt32(0);
                            r.Id_user = reader.GetInt32(1);
                            r.Descripcion = reader.GetString(2);
                            lista.Add(r);

                            Console.WriteLine("Id: " + r.Id);
                            Console.WriteLine("Id_user: " + r.Id_user);
                            Console.WriteLine("Descripcion: " + r.Descripcion);
                            Console.WriteLine("                   ");

                            
                        }

                        Console.WriteLine("Presione ENTER para volver al menu principal");
                        Console.ReadLine();
                        Console.Clear();
                        

                        break;
                        
                }
                
            } while(opt != "0");

        }
    }
}
